require './lib/ship.rb'
require './lib/cell.rb'
require './lib/grid.rb'
require './lib/player.rb'
require './lib/fleet.rb'
require './lib/helper.rb'



@ask = Helper.new()
answer = @ask.playGame()
name =""
if (answer == "y") || (answer == "Y")
  name = @ask.askPlayerName()
else
  @ask.goodBye()
end




@size = 5
@player1 = Player.new(name, @size)
@player2 = Player.new("Robot Overlord", @size)

@player1.placeShips()
@player2.placeShips()

@player1.createAttacker(@player2.getShipGrid)
@player2.createAttacker(@player1.getShipGrid)
@player1.toString()

player1FleetOK = true
player2FleetOK = true

while player2FleetOK && player1FleetOK do
  shotLocation = @player1.fireHumanShot(@ask)
  if shotLocation == "FLEET"
    @player1.toString()
    puts ""
    puts "The Robot Overlord has fired here!"
    puts ""
    @player2.getAttackString()
  else
    @player1.getAttackString()
    @player2.updateFleet(shotLocation)
    player2FleetOK = @player2.getStatus()
    shotLocation = @player2.fireShot()
    @player1.updateFleet(shotLocation)
    player1FleetOK = @player1.getStatus()
  end
end

@ask.clearScreen()
p1 = @player1.getShipsLeft()
p2 = @player2.getShipsLeft()
if p1 > p2
  puts "#{name} was victorious!!!! Well Done!!!\n"
else
  puts "The Robot Overload was victorious!!!! You drowned!!!\n"
end

@player1.toString()
@player2.toString()
