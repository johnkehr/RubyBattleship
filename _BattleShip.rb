
def game_window(size,ships,style)
    Shoes.app :width => 1200, :height => 600, :title => 'Game On!!!' do
        background navy
        hWidth  = width/2
        wHeight = height/2
        buff    = 15
        
        
        flow do  #************************************************************** 

            stack(margin:10, width:hWidth) do  # Attack grid
                @side1 = tagline strong("Attack Grid",stroke:white)
                @side1.displace(hWidth/2 - buff*4,0)
                

                
            end
               
            stack(margin:10, width:hWidth) do # Ship grid
                @side2 = tagline strong("Ship Grid",stroke:white)
                @side2.displace(hWidth/2 - buff*4,0)

                
            end

            
            
        end # Flow end
        
    end # Shoes.app end
end # Function end




def instruction_window()
    Shoes.app :width => 900, :height => 600, :title => 'Instruction Manual' do
        para File.read("Instructions.txt")        
    end # Shoes for instruction
end # end instruction_window



Shoes.app :width => 898, :height => 408, :title => 'The Ruby BattleShip' do
    background "rubySplash.jpeg"
    hWidth = width/2
    hHeight= height/2
    tWidth = width/10

    para "\n"
    
    flow do #**************************************************************        
        
        stack(margin:15, width:hWidth) do # The game options list_boxes ------
            @board = caption strong("Select board size:    ", stroke:red)
            list_box items: [5,8,10,12,15,20],
                choose: 10 do |list|@board = list.text
            end
            para "\n"        
            @style = caption strong("Select Game Style:    ", stroke:red)
            list_box items: ["Normal","Kinder","Obliteration","Linear"],
                choose: "Normal" do |list|@style = list.text
            end    
            para "\n"        
            @ships = caption strong("Number of ships:   ", stroke:red)
            list_box items: [1,2,3,4,5,6,7,8,9,10],
                choose: 5 do |list|@numShips = list.text
            end       
            
        end     #--------------------------------------------------------
        
        
        stack(margin:30, width:hWidth) do   # The start and instruction buttons ===
            para "\n\n"
            @startButton = button "Start Game!!!",width:0.35
            @startButton.displace(hWidth-(2*tWidth), 0)
            @startButton.click do
                game_window(@board,@numShips,@style)
            end
            para "\n\n"
            @tutorButton = button "Instructions.",width:0.35
            @tutorButton.displace(hWidth-(2*tWidth), 0)
            @tutorButton.click do
                instruction_window()
            end
            para "\n\n"        
        end   
    
    end    #**************************************************************
    
end #Shoes end
    
