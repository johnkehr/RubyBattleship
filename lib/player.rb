require './lib/ship.rb'
require './lib/cell.rb'
require './lib/grid.rb'
require './lib/auto_attack.rb'


class Player
	attr_accessor :status, :content
	attr_reader :size, :fleet

	def initialize(name, size)
		@name = name
		@size = size
		@baseShip = 5
		@myFleet = Fleet.new(@size)
		@shipGrid = Grid.new(@name)
		@attackGrid = Grid.new(@name)
	end


	def getShipGrid()
		return @shipGrid
	end

	def createAttacker(opposing)
		@blowThemUp = Attack.new(@attackGrid, opposing)
	end

	def createPlayerAttacker(opposing)
		@blowThemUp = Attack.new(@attackGrid, opposing)
	end

	def fireShot()
		@blowThemUp.nextAttack()
	end

	def fireHumanShot(asker)
		location = @blowThemUp.nextHumanAttack(asker)
		return location
	end

	def updateFleet(enemyFire)
		wasShip = @shipGrid.getShipType(enemyFire)
		if wasShip == "empty"
			@shipGrid.setMiss(enemyFire)
		else
			sunk = @myFleet.updateFleet(wasShip)
			if sunk
				puts "#{@name} lost their #{wasShip}"
				@size = @myFleet.getShipsLeft()
			end
		end
	end



	def placeShips()
		puts ""
		puts "#{@name} is placing ships..."
		@myFleet.place_ships(@shipGrid)
		puts "#{@name} has placed ships."
		puts ""
	end

	def getStatus()
		return @size > 0
	end

	def getShipsLeft()
		return @size
	end


	def getAttackString()
		@attackGrid.getAttackString()
	end


	def toString()
		puts @name
		puts "Currently has #{@size} ships"
		@myFleet.toString()
		@shipGrid.toString()
	end

end
