require './lib/ship.rb'


class Fleet

  attr_accessor
	attr_reader :size, :fleet

  def initialize(size)
    def red;            "\e[31m#{self}\e[0m" end
    @size = size
    @shipsLeft = size
    @fleet = []
    for i in 1..@size
      @fleet[i] = Ship.new(i)
    end
    @convert = [" ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"]
  end


  def toString()
    for i in 1..@size
      puts "#{fleet[i].name} has #{fleet[i].hit_count} hits."
    end
  end

  def getShipsLeft()
    return @shipsLeft
  end




  def updateFleet(name)
    sunk = false
    case name
    when "Carrier"
      @fleet[5].hit!
      sunk = @fleet[5].shipSunk()
    when "Battleship"
      @fleet[4].hit!
      sunk = @fleet[4].shipSunk()
    when "Destroyer"
      @fleet[3].hit!
      sunk = @fleet[3].shipSunk()
    when "Submarine"
      @fleet[2].hit!
      sunk = @fleet[2].shipSunk()
    when "Patrol"
      @fleet[1].hit!
      sunk = @fleet[1].shipSunk()
    end
    puts "#{name} was hit"
    if sunk
      @shipsLeft = @shipsLeft - 1
    end
    return sunk
  end



  def place_ships(shipGrid)
    maxRow = 10
    #placeRow
    for i in 1..@size
      shipPlaced = false
      while !shipPlaced do
        orientation = rand(2)
        finalPosition = 0
        location = getLocation(orientation, maxRow, @fleet[i].length)
        if orientation == 0
          direction = "vertically"
        else
          direction = "horizontally"
        end
        @fleet[i].setDirection(direction)
        shipPlaced = shipGrid.placeShip(@fleet[i], direction, location)
      end
      #puts "#{fleet[i].name} placed at #{location} in direction #{direction}"
    end
  end


  def getLocation(orientation, max, length)
    placeLetter = ""
    placeRow = 0
    placeCol = 0
    if orientation == 0   # zero orientation means up/down
      placeRow = (max - length) + 1
      placeRow = rand(placeRow) + 1
      placeCol = rand(max) + 1
    else                  # 1 orientation means left/right
      placeCol = (max - length) + 1
      placeCol = rand(placeCol) + 1
      placeRow = rand(max) + 1
    end
    placeLetter = @convert[placeRow]
    location = "#{placeLetter}#{placeCol}"
    return location
  end
end
