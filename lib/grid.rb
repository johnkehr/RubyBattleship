require './lib/ship.rb'
require './lib/cell.rb'

class Grid

	attr_accessor :cells
	attr_reader :name

	def initialize(playername)
		@cells = {}
		create_cells
		@name = "#{playername}"
		@vertical = "vertically"
		@horzizontal  = "horizontally"
		@convert = [" ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"]
		@top = "\033[35m	1	2	3	4	5	6	7	8	9	10\033[0m\n"
	end

	def create_cells
		("A".."L").each do |letter|
			(1..12).each do |number|
				k="#{letter}#{number}"
				@cells[k]=Cell.new(k)
			end
		end
	end


	def placeShip(ship, direction, location)
		length = ship.length
		finalLoction = 0
		row = location[0]
		rowNum = getRowNum(row)
		col = location[1].to_i
		isSet = false
		if direction == @horzizontal
			isSet = setHorizontal(rowNum, col, length, ship)
		else
			isSet = setVertical(col, rowNum, length, ship)
		end
		return isSet
	end


	def setHorizontal(rowNum, startLocation, length, ship)
		for i in 0..length-1
			location = "#{@convert[rowNum]}#{i+startLocation}"
			if @cells[location].isShip
				return false
			end
		end
		for i in 0..length-1
			location = "#{@convert[rowNum]}#{i+startLocation}"
			@cells[location].setShip(ship.getName)
			ship.setLocation(location)

		end
		return true
	end


	def setVertical(col, rowNum, length, ship)
		for i in 0..length-1
			location = "#{@convert[i+rowNum]}#{col}"
			if @cells[location].isShip
				return false
			end
		end
		for i in 0..length-1
			location = "#{@convert[i+rowNum]}#{col}"
			@cells[location].setShip(ship.getName)
			ship.setLocation(location)
		end
		return true
	end



	def getRowNum(letter)
		rowFound = false
		for i in 1..10
			if letter == @convert[i]
				return i
			end
		end
		return -1
	end


	def checkSpot(location)
		return @cells[location].checkHit
	end

	def updateGrid(location, shot)
		if shot == "hit"
			puts "#{@name} hit at #{location}!"
		end
		@cells[location].setHit(shot)
	end

	def getShipType(location)
		return @cells[location].getShipType()
	end

	def setMiss(location)
		@cells[location].setMiss()
	end



	def fire(coordinates)
		return  @cells[coordinates].fire()
	end


	def getAttackString()
		puts " #{@top}"
		line = ""
		("A".."J").each do |letter|
			line += " \033[35m#{letter}\033[0m"
			line += "\t"
			(1..10).each do |number|
				k="#{letter}#{number}"
				line += cells[k].attackString()
			end
			line += "\033[35m#{letter}\033[0m"
			puts line
			line = ""
		end
		puts " #{@top}"
	end




	def toString()
		puts " #{@top}"
		line = ""
		("A".."J").each do |letter|
			line += " \033[35m#{letter}\033[0m"
			line += "\t"
			(1..10).each do |number|
				k="#{letter}#{number}"
				line += cells[k].toString()
			end
			line += "\033[35m#{letter}\033[0m"
			puts line
			line = ""
		end
		puts " #{@top}"
	end



end
