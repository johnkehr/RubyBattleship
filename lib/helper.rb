class Helper



  def initialize()
    @name = ""
  end



  def clearScreen()
    for i in 0..50
      puts ""
    end
  end


  def askPlayerName()
    puts "What is your name?"
    puts ""
    @name = gets.chomp
    return @name
  end


  def goodBye()
    puts ""
    puts "Goodbye #{@name}!"
    puts ""
    puts ""
    abort()
  end

  def playGame()
    clearScreen()
    puts ""
    puts "Would you like to play a game? (y / n)"
    puts ""
    answer = gets.chomp
    clearScreen()
    puts ""
    puts "Do you need instructions? (y / n)"
    puts ""
    result = gets.chomp
    if result == "Y" || result == "y"
      showInstructions()
    end
    return answer
  end

  def getNextPosition()
    puts ""
    puts "#{@name}, please select your next shot."
    puts ""
    location = gets.chomp
    location = location.upcase
    location = options(location)
    return location
  end


  def showInstructions()
    puts ""
    puts "type q to quit game"
    puts ""
    puts "type fleet to see your fleet status"
    puts ""
    puts "enter locations of row and column, for example a1 or j10"
    puts ""
    puts "AC = Aircraft Carrier"
    puts "Ba = Battleship"
    puts "De = Destroyer"
    puts "Su = Submarine"
    puts "Pa = Patrol Boat"
    puts "hit enter to continue"
    puts "\033[31mRed   = Hit!\033[0m"
    puts "\033[34mBlue  = Not hit!\033[0m"
    puts "White = Miss!"
    puts ""
    puts ""
    puts "Hit enter to continue."
    ready = gets.chomp
  end

  def options(humanNeedsHelp)
    case humanNeedsHelp
    when "Q"
      goodBye()
    end
    return humanNeedsHelp
  end


end
