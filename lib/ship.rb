
class Ship

	attr_accessor :hit_count, :name, :direction
	attr_reader :length, :name, :image

	def initialize(length)
		@length		= length
		@hit_count	= 0
		@placed		= false
		@loc_place = 0
		@locations = {}
		@sunk = false
    case length
      when 5
				@name = "Carrier"
				@image = "./assets/aircraftcarrier.png"
			when 4
				@name = "Battleship"
				@image = "./assets/battleship.png"
			when 3
				@name = "Destroyer"
				@image = "./assets/destroyer.png"
			when 2
				@name = "Submarine"
				@image = "./assets/submarine.png"
				@length = 3
			when 1
				@name = "Patrol"
				@image = "./assets/patrol.png"
				@length = 2
			end
	end

	def getName
		return @name
	end

	def setDirection(direction)
		@direction = direction
	end

	def setLocation(location)
		@locations[@loc_place] = location
		@loc_place = @loc_place + 1
	end

	def hit!
		@hit_count += 1
		if @hit_count == @length
			@sunk = true
		end
	end

	def placed?
		@placed
	end

	def place!
		@placed = true
	end

	def shipSunk()
		return @sunk
	end

end
