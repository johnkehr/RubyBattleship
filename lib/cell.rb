class Cell

	attr_accessor :status, :content
	attr_reader :coordinates

	def initialize(coordinates)
		@status = "notHit"
		@content = "water"
		@ship = "empty"
		@coordinates = coordinates
		def red;            "\e[31m#{self}\e[0m" end
	end

	def setShip(shipName)
		@content = "ship"
		@ship = shipName
	end

	def isShip
		if @content == "water"
			return false
		end
		return true
	end


	def checkHit()
		if @status == "notHit"
			return false
		else
			return true
		end
	end


	def setHit(toTrack)
		@status = toTrack
	end

	def setMiss()
		@status = "miss"
	end


	def fire()
		if @content == "water"
			@status = "miss"
		else
			@status = "hit"
		end
		return @status
	end

	def getShipType
		return @ship
	end


	def attackString()
		result = ""
		if @status == "notHit"
			result += "\033[34m*\033[0m"
		elsif @status == "hit"
			result += "\033[31mH\033[0m"
		else
			result += "M"
		end
		result += "\t"
		return result
	end


	def toString()
		result = ""
		case @ship
		when "Carrier"
			if @status == "notHit"
				result += "\033[34mAC\033[0m"
			else
				result += "\033[31mAC\033[0m"
			end
		when "Battleship"
			if @status == "notHit"
				result += "\033[34mBa\033[0m"
			else
				result += "\033[31mBa\033[0m"
			end
		when "Destroyer"
			if @status == "notHit"
				result += "\033[34mDe\033[0m"
			else
				result += "\033[31mDe\033[0m"
			end
		when "Submarine"
			if @status == "notHit"
				result += "\033[34mSu\033[0m"
			else
				result += "\033[31mSu\033[0m"
			end
		when "Patrol"
			if @status == "notHit"
				result += "\033[34mPa\033[0m"
			else
				result += "\033[31mPa\033[0m"
			end
		when "empty"
			if @status == "miss"
				result += "**"
			else
				result += "\033[34m**\033[0m"
			end
		end
		result += "\t"
		return result
	end


end
