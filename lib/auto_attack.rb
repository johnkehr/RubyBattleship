require './lib/helper.rb'

class Attack


  def initialize(trackingGrid, toAttackGrid)
    @trackingGrid = trackingGrid
    @oppsingGrid  = toAttackGrid
    @max = 10
    @convert = [" ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"]
  end


  def nextAttack()
    goodLocation = false
    count = 0
    while !goodLocation && (count <= 500) do
      location = getLocation
      hasBeenHit = @trackingGrid.checkSpot(location)
      if !hasBeenHit
        goodLocation = true
      end
      count = count + 1
    end

    shot = @oppsingGrid.fire(location)
    @trackingGrid.updateGrid(location,shot)
    return location
  end

  def nextHumanAttack(asker)
    location = asker.getNextPosition()
    if location == "FLEET"
      return location
    end
    
    hasBeenHit = @trackingGrid.checkSpot(location)
    while hasBeenHit
      puts "You already hit there, try again."
      location = asker.getNextPosition()
      hasBeenHit = @trackingGrid.checkSpot(location)
    end
    shot = @oppsingGrid.fire(location)
    @trackingGrid.updateGrid(location, shot)
    return location
  end


  def getLocation()
    placeLetter = ""
    hitRow = rand(@max-1) + 1
    hitCol = rand(@max-1) + 1
    hitLetter = @convert[hitRow]
    location = "#{hitLetter}#{hitCol}"
    return location
  end




end
