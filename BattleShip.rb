require 'gameWindowGrid.rb'


def instruction_window()
    Shoes.app :width => 900, :height => 600, :title => 'Instruction Manual' do
        para File.read("assets/Instructions.txt")
    end # Shoes for instruction
end # end instruction_window



Shoes.app :width => 898, :height => 408, :title => 'The Ruby BattleShip' do
    background "assets/rubySplash.jpeg"
    hWidth = width/2
    hHeight= height/2
    tWidth = width/10

    para "\n"

    flow do #**************************************************************

        stack(margin:15, width:hWidth) do # The game options list_boxes ------
            @board = caption strong("Select board size:    ", stroke:red)
            list_box items: [10],
                choose: 10 do |list|@board = list.text
            end
            para "\n"
            @style = caption strong("Select Game Style:    ", stroke:red)
            list_box items: ["Normal","Kinder","Obliteration","Linear"],
                choose: "Normal" do |list|@style = list.text
            end
            para "\n"
            @ships = caption strong("Number of ships:   ", stroke:red)
            list_box items: [1,2,3,4,5,6,7,8,9,10],
                choose: 5 do |list|@numShips = list.text
            end

        end     #--------------------------------------------------------


        stack(margin:30, width:hWidth) do   # The start and instruction buttons ===
            para "\n\n"
            @startButton = button "Start Game!!!",width:0.35
            @startButton.displace(hWidth-(2*tWidth), 0)
            @startButton.click do
                actual_game_window(@board,@numShips,@style)
            end
            para "\n\n"
            @tutorButton = button "Instructions.",width:0.35
            @tutorButton.displace(hWidth-(2*tWidth), 0)
            @tutorButton.click do
                instruction_window()
            end
            para "\n\n"
        end

    end    #**************************************************************

end #Shoes end
