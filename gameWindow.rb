def top_game_window(hWidth,buff)
    flow do  #**************************************************************
            stack(margin:10, width:hWidth) do  # Attack grid
                @side1 = tagline strong("Attack Grid",stroke:white)
                @side1.displace(hWidth/2 - buff*4,0)
            end
            stack(margin:10, width:hWidth) do # Ship grid
                @side2 = tagline strong("Ship Grid",stroke:white)
                @side2.displace(hWidth/2 - buff*4,0)
            end
        end # Flow end
end
def gridSquare(x,y, gridSize)
    @x = x
    @y = y
    @gridSquareSize = gridSize
    @ship = nil
    @gridsquare = rect :left => @x, :top => @y,
    :width => @gridSquareSize, :height =>@gridSquareSize
  end
def init_ship_grid(grid, size,hWidth)
  flow margin: 10, width: 0.5 do
      buttonSize = (hWidth-50) / size
      stroke white
      fill paleturquoise
      for i in 0..size-1
        grid_row = Array[]
        for j in 0..size-1
          @gridSquare = gridSquare((i)*buttonSize, (j)*buttonSize, buttonSize)
          grid_row <<@gridSquare # appends items << is like push
          end
          grid <<grid_row
        end
     end
    return grid
end
def place_your_ships(shipGrid, hWidth, size)
   if(@size == 1)
     @image= image "assets/patrol.png"
     end
   if(@size == 2)
     @image= image "assets/submarine.png"
   end
   if(@size == 3)
     @image= image "assets/destroyer.png"
     end
   if(@size == 4)
    @image= image "assets/battleship.png"
     end
   if(@size == 5)
     @image= image "assets/aircraftcarrier.png"
     end
   x, y = self.mouse

  # for @size in 1..5
  click do |gridSquare, x, y|
    if((hWidth+10..hWidth*2).include? x)
      @image= image "assets/aircraftcarrier.png"
    @image.left = x- x % @gridSquareSize -5
    @image.top = y- y % @gridSquareSize +8
      #  rect x-5, y+8, @gridSquareSize, @gridSquareSize, :fill => @image
      #  @gridSquare.at(0).at(0).fill(black)
    #end
    end
    end
end
def init_dialog(hWidth)
    stack do
      para "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
      para @status.replace "Place your ships!  Single patrol boat-1  Submarine-2 Destroyer-3 Battleship-4 Carrier-5"
      para @instructions.replace "Enter size."
      @edit = edit_line width: 400
      button "ok" do
        @size = @edit.text
      end
      place_your_ships(@shipGrid, hWidth, @size)
      end
end
  Shoes.app title:'Game On!!!', width:(1400), height:(800), resizable: false do
    background navy..white
    hWidth  = width/2
    wHeight = height/2
    buff    = 15
    @size = 10
    @status = "Let's get started!"
    @instructions = ""
    flow do  #Start window flow***************************************
         @top_game_window =  top_game_window(hWidth,buff);
         @attackGrid = Array[]
         @shipGrid = Array[]
        # @attackGrid = init_ship_grid(@size, hWidth);
        init_ship_grid(@attackGrid, @size, hWidth);
        init_ship_grid(@shipGrid, @size, hWidth);
        init_dialog(hWidth)
        end
end # Shoes.app end
