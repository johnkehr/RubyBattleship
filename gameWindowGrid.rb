
def top_game_window(hWidth,buff)
    flow do  #**************************************************************
            stack(margin:10, width:hWidth) do  # Attack grid
                @side1 = tagline strong("Attack Grid",stroke:white)
                @side1.displace(hWidth/2 - buff*4,0)

            end
            stack(margin:10, width:hWidth) do # Ship grid
                @side2 = tagline strong("Ship Grid",stroke:white)
                @side2.displace(hWidth/2 - buff*4,0)
            end

        end # Flow end
end

def gridSquare(x,y, gridSize)
    @x = x
    @y = y
    @gridSquareSize = gridSize
    @ship = nil
    @gridsquare = rect :left => @x, :top => @y,
    :width => @gridSquareSize, :height =>@gridSquareSize
  end

def init_ship_grid(grid, size, hWidth)
  flow margin: 10, width: 0.5 do
      stroke white
      fill paleturquoise
      for i in 0..size-1
        grid_row = Array[]
        for j in 0..size-1
          @gridSquare = gridSquare((i)*@buttonSize, (j)*@buttonSize, @buttonSize)
          grid_row << @gridSquare # appends items << is like push
        end
        grid << grid_row
      end
   end
   return grid
end

def place_your_ships(shipGrid, hWidth, length)
  x, y = self.mouse
  click do |gridSquare, x, y|
      if((hWidth+10..hWidth*2).include? x)
        @left = x- x % @buttonSize
        @top = y- y % @buttonSize
      @ship = rect @left-5, @top+8, @buttonSize*length, @buttonSize, :fill => gray
    #  if((hWidth+10..(hWidth*2 - @buttonSize*length)).include?  @ship.left)
      keypress do |k|
        if (@ship.width == @buttonSize*length)||(@ship.height == @buttonSize)
            @newWidth = @buttonSize
            @newHeight =  @buttonSize*length
           else
            @newWidth =@buttonSize*length
            @newHeight =  @buttonSize
          end
        case k
        when :left  then  @ship.left = @ship.left - @buttonSize
        when :right then   @ship.left = @ship.left + @buttonSize
        when :up  then  @ship.top = @ship.top - @buttonSize
        when :down then   @ship.top = @ship.top + @buttonSize
        when " " then (@ship.height =  @newHeight)&&(@ship.width = @newWidth)
      end
      end
    end
  end
end

def init_dialog(hWidth)
  flow :margin => 4 do
    para "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n
    Select a ship and place on board. Use the arrow keys to move them and the space bar to rotate.\n"
      button("Destroyer-2")  do
        place_your_ships(@shipGrid, hWidth, 2)
        end
      button("Submarine-3") do
        place_your_ships(@shipGrid, hWidth, 3)
        end
      button("Cruiser-3") do
        place_your_ships(@shipGrid, hWidth,  3)
        end
      button("Battleship-4") do
        place_your_ships(@shipGrid, hWidth,  4)
        end
      button("Carrier-5") do
        place_your_ships(@shipGrid, hWidth,  5)
        end
      end
end
#==================================================================================================
# The SHOES application
#==================================================================================================

def actual_game_window(size,ships,style)
	Shoes.app title:'Game On!!!', width:(1400), height:(850), resizable: false do
    	background navy..white
    	hWidth  = width/2
    	wHeight = height/2
    	buff    = 15
    	@size = size
		  @ships = ships
	  	@buttonSize = (hWidth-50) / size
    	@status = "Let's get started!"
    	@instructions = ""

      flow do  #Start window flow***************************************
           @top_game_window =  top_game_window(hWidth,buff);
           @attackGrid = Array[]
           @shipGrid = Array[]
          init_ship_grid(@attackGrid, @size, hWidth);
          init_ship_grid(@shipGrid, @size, hWidth);
          init_dialog(hWidth)
          end
  end
end

#==================================================================================================
