This is a classic game of battleship, setup to play against a computer. At start the ships are placed randomly on a grid and then guess the location of the opposing fleet and sink all their ships before  Each grid unit is marked as a hit or miss when a guess is made.
Files
playerBased.rb – Driver class
/lib/player.rb – Defines the players of the game
/lib/fleet.rb - Defines the fleet of ships
/lib/ship.rb – Ship information
/lib/grid.rb – Game grid. Used for both the ships and the shot tracking
/lib/cell.rb – Cell status. Knows type of ship, and hit/miss status.
/lib/auto_attack.rb - Manages the robot attacks and the player attacks
/lib/helper.rb - helps with player interactions.
README.md- This file

For unsuccessful GUI
Battleship.rb
gameWindowGrid.rb
/assets/Instructions.txt
/assets/rubySplash.jpeg


Installation

Console
You will need Ruby installed to run the console version. Ruby comes preinstalled on most linux and mac os. To see the version installed, use the command:
ruby-v
If it is not already installed, go here to download.
GUI
For the GUI version you will need Shoes, go here to download.

Usage
Console
Use cd to get into the directory the project folder is in. To get started use the following command:
shoes BattleShip.rb #Not functional, but will show progress
ruby playerBased.rb  #to play console game



GUI
Once shoes is set up, you can open the program and click on 
Run an App
Find the file Battleship.rb and click to get started.

Authors
John Kehr
Craig Morford
Lois Baek

